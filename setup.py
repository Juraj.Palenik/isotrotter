import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="isotrotter", 
    version="0.3.0",
    author="Juraj Palenik",
    author_email="Juraj.Palenik@uib.no",
    description="Atmospheric convection analysis tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.app.uib.no/Juraj.Palenik/isotrotter",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'matplotlib',
        'numpy',
        'scipy',
        'xarray',
        'netCDF4',
        'pandas',
        'metpy',
        'pyproj',
        'pyqt5',
        'pyopengl',
        'pyqtgraph',
        'tqdm'
      ],
    python_requires='>=3.6',
)